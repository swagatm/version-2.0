package com.appnomic.appsone.MySQL;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Data
public class KpiDetailsBean {
    private int id;
    private String name;
    private int componentId;
    private int typeId;
    private String clusterOperation;
    private String measureUnits;
    private String dataType;
    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
}
