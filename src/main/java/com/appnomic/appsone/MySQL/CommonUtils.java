package com.appnomic.appsone.MySQL;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CommonUtils {
    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    private CommonUtils() {
        //Dummy constructor
    }


    public static String getDecryptedData(String encryptedData) {
        //TODO BouncyCastle encryption algorithm has to be used in further releases.
        return new String(Base64.getDecoder().decode(encryptedData));
    }




}

class EscapeHTML extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        String s = jp.getValueAsString();
        return StringEscapeUtils.escapeHtml4(s);
    }
}
