package com.appnomic.appsone.MySQL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.IOException;

public class InitializeConstants {
    public static String poolSize;
    public static String maxPoolSize;
    public static String waitTime;
    public static String driverName;
    public static String h2DriverName;
    public static String url;
    public static String username;
    public static String encryptedPassword;

    public static void initializeConstants(String configPath){
        JSONObject jsonObject = readConfig(configPath);
         poolSize = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.db.pool.size");
         maxPoolSize = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.db.pool.size.max");
        waitTime = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.db.max.wait");
        driverName = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.driver.name");
        h2DriverName = (String) ((JSONObject)jsonObject.get("percona_config")).get("h2.driver.name");
        url = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.server.connection.url");
        username = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.database.username");
        encryptedPassword = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.database.password");
        if(poolSize.equals("")){
            poolSize = Constants.MYSQL_DB_POOL_SIZE_DEFAULT;
        }
        if(maxPoolSize.equals("")){
            maxPoolSize = Constants.MYSQL_DB_MAX_POOL_SIZE_DEFAULT;
        }
        if(waitTime.equals("")){
            waitTime = Constants.MYSQL_DB_MAX_WAIT_DEFAULT;
        }
        if(driverName.equals("")){
            driverName = Constants.MYSQL_DB_DRIVER_NAME_DEFAULT;
        }
        if(h2DriverName.equals("")){
            h2DriverName = Constants.H2_DB_DRIVER_NAME_PROPERT_DEFAULT;
        }
        if(url.equals("")){
            url =  Constants.MYSQL_DB_CONNECTION_URL_DEFAULT;
        }
        if(username.equals("")){
            username = Constants.MYSQL_DB_USERNAME_PROPERTY_DEFAULT;
        }
        if(encryptedPassword.equals("")){
            encryptedPassword = Constants.MYSQL_DB_PASSWORD_PROPERTY_DEFAULT;
        }

    }
    private static JSONObject readConfig(String arg) {
        String ConfigFile = arg;
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
