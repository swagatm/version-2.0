package com.appnomic.appsone.MySQL;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

public enum MySQLConnectionManager {
    INSTANCE;
    private boolean haveToRunTestCases = false;
    private boolean isIntegrationTest = false;
    private static final Logger log = LoggerFactory.getLogger(MySQLConnectionManager.class);
    private static String H2URL = "jdbc:h2:mem:apmcommon;DB_CLOSE_DELAY=-1;MODE=MySQL;INIT=RUNSCRIPT FROM './src/test/resources/create.sql'\\;RUNSCRIPT FROM './src/test/resources/views.sql'\\;RUNSCRIPT FROM './src/test/resources/populate.sql'";

    private final Integer poolSize = Integer.parseInt(InitializeConstants.poolSize);
    private final Integer maxPoolSize = Integer.parseInt(InitializeConstants.maxPoolSize);
    private final Integer waitTime = Integer.parseInt(InitializeConstants.waitTime);
    private final String driverName = InitializeConstants.driverName;
    private final String h2DriverName = InitializeConstants.h2DriverName;

    private volatile int testIdlePoolSize = 1;
    private boolean testIdlePoolSizeLock = false;

    public static MySQLConnectionManager getInstance() { return INSTANCE; }
    
    private DBI dbi = null;

    public DBI getDbi() {
        return dbi;
    }

    public void setDbi(DBI dbi) {
        this.dbi = dbi;
    }

    public String getH2URL() {
        return H2URL;
    }

    public void setH2URL(String h2URL) {
        MySQLConnectionManager.H2URL = h2URL;
    }

    public DBI getHandle()  {
        log.debug("Getting the DB Handle");
        if(dbi != null){
            return dbi;
        }
        initializeDBI();
        log.debug("Got the DB Handle, hence returning");
        return  dbi;
    }

    private synchronized void initializeDBI(){
        if(dbi == null){
            DataSource dataSource = createDataSource();
            dbi = new DBI(dataSource);
        }
    }


    public <JdbiDao> JdbiDao open(Class<JdbiDao> dao) {
        initializeDBI();
        return dbi.onDemand(dao);
    }

    public <JdbiDao> void close(JdbiDao dao) {
        if (dbi != null) dbi.close(dao);
    }


    private DataSource createDataSource() {

        //for unit test cases
        if (haveToRunTestCases) {
            log.error("creating test data source");
            return createTestDatasource();
        }
        String url = InitializeConstants.url;
        String username = InitializeConstants.username;
        String encryptedPassword = InitializeConstants.encryptedPassword;
        //System.out.println(encryptedPassword);
        String[] content = url.split("/");
        String[] schemaNameContent = content[3].split("\\?");
        String schemaName = schemaNameContent[0];

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driverName);
        if (isIntegrationTest) {
            hikariConfig.setJdbcUrl(url);
            if (schemaName.equalsIgnoreCase("appsone")) {
                hikariConfig.setJdbcUrl(url.replace("appsone", "appsone_integration_test"));
            }
        } else {
            hikariConfig.setJdbcUrl(url);
        }
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(CommonUtils.getDecryptedData(encryptedPassword));
        //System.out.println(CommonUtils.getDecryptedData(encryptedPassword));
        hikariConfig.setAutoCommit(true);
        hikariConfig.setMinimumIdle(poolSize);
        hikariConfig.setMaximumPoolSize(maxPoolSize);
        return new HikariDataSource(hikariConfig);

    }

    private DataSource createTestDatasource()   {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(h2DriverName);
        hikariConfig.setJdbcUrl(H2URL);
        hikariConfig.setUsername("sa");
        hikariConfig.setPassword("");
        hikariConfig.setAutoCommit(true);
        hikariConfig.setMinimumIdle(this.testIdlePoolSize);
        hikariConfig.setMaximumPoolSize(10);
        return new HikariDataSource(hikariConfig);
    }

    public boolean setHaveToRunTestCases(boolean haveToRunTestCases) {
        this.haveToRunTestCases = haveToRunTestCases;
        return haveToRunTestCases;
    }

    public void setIntegrationTestStatus(boolean isIntegrationTest){
        log.info("integration test:::::::::::::::::::::::::");
        this.isIntegrationTest = isIntegrationTest;
    }

    public boolean isIntegrationTest(){
        return isIntegrationTest;
    }

    public void setTestIdlePoolSize(int testIdlePoolSize) {
        synchronized (this) {
            if( ! this.testIdlePoolSizeLock ) {
                this.testIdlePoolSize = testIdlePoolSize;
                this.testIdlePoolSizeLock = true;
            }
        }
    }

    public void unlockTestIdlePoolSizeSetter() {
        synchronized (this) {
            this.testIdlePoolSizeLock = false;
        }
    }

}
