package com.appnomic.appsone.MySQL;

import com.appnomic.appsone.MySQL.KpiDetailsBean;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.List;


@UseStringTemplate3StatementLocator
public interface KPIDataDao {
    @RegisterMapperFactory(BeanMapperFactory.class)
    @SqlQuery("SELECT * FROM mst_kpi_details")
    List<KpiDetailsBean> getKpiList();
}

