package com.appnomic.appsone.MySQL;

public class Constants {
    private Constants(){
    }
    /**
     * Messages
     */
    public static final String MYSQL_DB_CONNECTION_URL_PROPERTY_NAME = "mysql.server.connection.url";
    public static final String MYSQL_DB_CONNECTION_URL_DEFAULT = "jdbc:mysql://127.0.0.1:3306/apmcommon?zeroDateTimeBehavior=convertToNull";

    public static final String MYSQL_DB_USERNAME_PROPERTY_NAME = "mysql.database.username";
    public static final String MYSQL_DB_USERNAME_PROPERTY_DEFAULT = "appsone";
    // public static final String MYSQL_DB_AUTH_PROPERTY_NAME = "mysql.database.password";
    // public static final String MYSQL_DB_AUTH_PROPERTY_DEFAULT = "QXBwJDBuZQ==";//13.44
    //public static final String MYSQL_DB_AUTH_PROPERTY_DEFAULT = "YWxjQDEyMw==";//13.65
    // public static final String MYSQL_DB_AUTH_PROPERTY_DEFAULT = "QHBwbjBtIWNfMQ==";//13.127

    public static final String MYSQL_DB_PASSWORD_PROPERTY_NAME = "mysql.database.password";
    public static final String MYSQL_DB_PASSWORD_PROPERTY_DEFAULT = "QXBwJDBuZQ==";//13.44
    //public static final String MYSQL_DB_PASSWORD_PROPERTY_DEFAULT = "YWxjQDEyMw==";//13.65
    // public static final String MYSQL_DB_PASSWORD_PROPERTY_DEFAULT = "QHBwbjBtIWNfMQ==";//13.127

    public static final String MYSQL_DB_POOL_SIZE_PROPERTY_NAME = "mysql.db.pool.size";
    public static final String MYSQL_DB_POOL_SIZE_DEFAULT = "50";

    public static final String MYSQL_DB_MAX_POOL_SIZE_PROPERTY_NAME = "mysql.db.pool.size.max";
    public static final String MYSQL_DB_MAX_POOL_SIZE_DEFAULT = "100";

    public static final String MYSQL_DB_MAX_WAIT_PROPERTY_NAME = "mysql.db.max.wait";
    public static final String MYSQL_DB_MAX_WAIT_DEFAULT = "10000";

    public static final String MYSQL_DB_DRIVER_NAME_PROPERTY_NAME = "mysql.driver.name";
    public static final String MYSQL_DB_DRIVER_NAME_DEFAULT = "com.mysql.cj.jdbc.Driver";

    public static final String H2_DB_DRIVER_NAME_PROPERT = "h2.driver.name";
    public static final String H2_DB_DRIVER_NAME_PROPERT_DEFAULT = "org.h2.Driver";

    public static final String COMPONENT_TYPE_DB_SERVER = "Database Server";
}
