package com.appnomic.appsone.Cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.Session;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CassandraConnectionManager {
    private static final String nodesKey = "nodes";
    private static final String keyspaceKey = "keyspace_names";
    private static final String usernameKey = "username";
    private static final String authKey = "password";
    private static final String connectionpoolKey = "maxConnections";
    private static final String sslEnabled = "sslEnabled";
    private static final String threadPoolMin = "2";
    private static final String threadPoolMax = "10";
    private static Session cassandraSession = null;

    private static final Logger logger = LoggerFactory.getLogger(CassandraConnectionManager.class);

    public static Session getSession(String configPath) {
        Map<String,Object> cassandraConfigs = getCassandraConfigParams(configPath);
        List<InetSocketAddress> nodes = (List<InetSocketAddress>) cassandraConfigs.get(nodesKey);
        String cassandraKeySpaceName = (String) cassandraConfigs.get(keyspaceKey);
        String cassandraUsername = (String) cassandraConfigs.get(usernameKey);
        String cassandraPassword = (String) cassandraConfigs.get(authKey);
        int cassandraConnectionpoolSize = Integer.valueOf((String) cassandraConfigs.get(connectionpoolKey));
        boolean isSslEnabled = Boolean.parseBoolean((String)cassandraConfigs.get(sslEnabled));


        try {
            if(cassandraSession == null || cassandraSession.isClosed())    {
                PoolingOptions poolingOptions = new PoolingOptions().setMaxConnectionsPerHost(HostDistance.REMOTE, cassandraConnectionpoolSize);
                if(isSslEnabled){
                    cassandraSession = Cluster.builder()
                            .addContactPointsWithPorts(nodes)
                            .withCredentials(cassandraUsername, cassandraPassword)
                            .withPoolingOptions(poolingOptions)
                            .withSSL()
                            .build()
                            .init()
                            .connect(cassandraKeySpaceName);
                } else {
                    cassandraSession = Cluster.builder()
                            .addContactPointsWithPorts(nodes)
                            .withCredentials(cassandraUsername, cassandraPassword)
                            .withPoolingOptions(poolingOptions)
                            .build()
                            .init()
                            .connect(cassandraKeySpaceName);
                }
            }
        } catch (Exception e) {
            logger.error("Unable to get a connection to cassandra.",e);
            return null;
        }
        return cassandraSession;
    }

    private static Map<String,Object> getCassandraConfigParams(String configPath) {
        JSONObject jsonObject = readConfig(configPath);
        String cassandraKeyspaceName = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.server.schema");
        String cassandraIpPortList = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.nodes");
        String cassandraUsername = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.server.username");
        String cassandraPassword = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.server.password");
        String cassandraConnectionpoolSize = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.connectionpool.size");
        String cassandraSslEnabled = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.ssl.enabled");
        String threadPoolMinSize = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("threadpool.min.size");
        String threadPoolMaxSize = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("threadpool.max.size");
        if(cassandraKeyspaceName.equals("")){
            cassandraKeyspaceName = Constants.CASSANDRA_SERVER_DEFAULT_SCHEMA;
        }
        if(cassandraIpPortList.equals("")){
            cassandraIpPortList = Constants.CASSANDRA_NODES_DEFAULT_VALUE;
        }
        if(cassandraUsername.equals("")){
            cassandraUsername = Constants.CASSANDRA_SERVER_USERNAME_DEFAULT_VALUE;
        }
        if(cassandraPassword.equals("")){
            cassandraPassword = Constants.CASSANDRA_SERVER_AUTH_DEFAULT_VALUE;
        }
        if(cassandraConnectionpoolSize.equals("")){
            cassandraConnectionpoolSize = Constants.CASSANDRA_SERVER_CONNECTIONPOOL_SIZE_DEFAULT;
        }
        if(cassandraSslEnabled.equals("")){
            cassandraSslEnabled = Constants.CASSANDRA_SSL_ENABLED_DEFAULT;
        }
        if(threadPoolMinSize.equals("")){
            threadPoolMinSize = Constants.THREAD_POOL_MIN_SIZE_DEFAULT_VALUE;
        }
        if(threadPoolMaxSize.equals("")){
            threadPoolMaxSize = Constants.THREAD_POOL_MAX_SIZE_DEFAULT_VALUE;
        }
//        System.out.println(cassandraKeyspaceName);
//        System.out.println(cassandraIpPortList);
//        System.out.println(cassandraUsername);
//        System.out.println(cassandraPassword);
//        System.out.println(cassandraConnectionpoolSize);
//        System.out.println(cassandraSslEnabled);
//        System.out.println(threadPoolMinSize);
//        System.out.println(threadPoolMaxSize);


//        String cassandraKeyspaceName = ConfProperties.getString(Constants.CASSANDRA_SERVER_SCHEMA_PROPERTY_NAME,
//                Constants.CASSANDRA_SERVER_DEFAULT_SCHEMA);
//        String cassandraIpPortList = ConfProperties.getString(Constants.CASSANDRA_NODES_PROPERTY_NAME,
//                Constants.CASSANDRA_NODES_DEFAULT_VALUE);
//        String cassandraUsername = ConfProperties.getString(Constants.CASSANDRA_SERVER_USERNAME_PROPERTY_NAME,
//                Constants.CASSANDRA_SERVER_USERNAME_DEFAULT_VALUE);
//        String cassandraPassword = ConfProperties.getString(Constants.CASSANDRA_SERVER_AUTH_PROPERTY_NAME,
//                Constants.CASSANDRA_SERVER_AUTH_DEFAULT_VALUE);
//        String cassandraConnectionpoolSize = ConfProperties.getString(Constants.CASSANDRA_SERVER_CONNECTIONPOOL_SIZE,
//                Constants.CASSANDRA_SERVER_CONNECTIONPOOL_SIZE_DEFAULT);
//        String cassandraSslEnabled = ConfProperties.getString(Constants.CASSANDRA_SSL_ENABLED,
//                Constants.CASSANDRA_SSL_ENABLED_DEFAULT);

        String[] list = cassandraIpPortList.trim().split(",");

        List<InetSocketAddress> nodes = new ArrayList<InetSocketAddress>();

        for (String nodeIpPort : list) {

            String[] ipPort = nodeIpPort.trim().split(":", 3);
            String cassandraIp = ipPort[0];
            int cassandraPort = (ipPort.length > 1) ? Integer.parseInt(ipPort[1]) : Constants.CASSANDRA_SERVER_DEFAULT_PORT;
            InetSocketAddress node = new InetSocketAddress(cassandraIp, cassandraPort);

            nodes.add(node);
        }

        Map<String,Object> result = new HashMap<>();
        result.put(nodesKey, nodes);
        result.put(keyspaceKey, cassandraKeyspaceName);
        result.put(usernameKey, cassandraUsername);
        result.put(authKey,cassandraPassword);
        result.put(connectionpoolKey, cassandraConnectionpoolSize);
        result.put(sslEnabled, cassandraSslEnabled);
        result.put(threadPoolMin, threadPoolMinSize);
        result.put(threadPoolMax, threadPoolMaxSize);
        return result;
    }

    private static JSONObject readConfig(String arg) {
        String ConfigFile = arg;
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
