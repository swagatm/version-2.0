package com.appnomic.appsone.Cassandra;

public class Constants {


    private Constants(){

    }
    /**
     * Messages
     */
    public static final String CONF_PROPERTIES_FILE_NAME = "conf.properties";
    public static final String KEYCLOAK_SSO_CONF_FILE_NAME = "keycloak_details.json";
    public static final Integer CASSANDRA_SERVER_DEFAULT_PORT = 9042;
    public static final String CASSANDRA_SERVER_SCHEMA_PROPERTY_NAME = "cassandra.server.schema";
    public static final String CASSANDRA_SERVER_DEFAULT_SCHEMA = "appsone";

    public static final String CASSANDRA_NODES_PROPERTY_NAME = "cassandra.nodes";
    public static final String CASSANDRA_NODES_DEFAULT_VALUE = "127.0.0.1:9042";

    public static final String CASSANDRA_SERVER_USERNAME_PROPERTY_NAME = "cassandra.server.username";
    public static final String CASSANDRA_SERVER_USERNAME_DEFAULT_VALUE = "";

    public static final String CASSANDRA_SERVER_AUTH_PROPERTY_NAME = "cassandra.server.password";
    public static final String CASSANDRA_SERVER_AUTH_DEFAULT_VALUE = "";

    public static final String CASSANDRA_SERVER_CONNECTIONPOOL_SIZE = "cassandra.connectionpool.size";
    public static final String CASSANDRA_SERVER_CONNECTIONPOOL_SIZE_DEFAULT = "10";

    public static final String CASSANDRA_SSL_ENABLED = "cassandra.ssl.enabled";
    public static final String CASSANDRA_SSL_ENABLED_DEFAULT = "true";

    public static final String THREAD_POOL_MIN_SIZE_NAME = "threadpool.min.size";
    public static final String THREAD_POOL_MIN_SIZE_DEFAULT_VALUE = "2";

    public static final String THREAD_POOL_MAX_SIZE_NAME = "threadpool.max.size";
    public static final String THREAD_POOL_MAX_SIZE_DEFAULT_VALUE = "10";
}
