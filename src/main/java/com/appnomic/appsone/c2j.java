package com.appnomic.appsone;

import com.appnomic.appsone.Cassandra.CassandraConnectionManager;
import com.appnomic.appsone.MySQL.InitializeConstants;
import com.appnomic.appsone.MySQL.KPIDataDao;
import com.appnomic.appsone.MySQL.KpiDetailsBean;
import com.appnomic.appsone.MySQL.MySQLConnectionManager;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.BuiltStatement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import org.apache.log4j.BasicConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class c2j {
    public static void main(String[] args) throws ParseException {
        System.out.println ("Finding DataGaps for a given configuration");
        BasicConfigurator.configure();
        //String pwd = new File(System.getProperty("user.dir")).getParent();
        String ConfigPath = args[0];
        String resultPath = args[1];
        JSONObject jsonObject = readConfig(ConfigPath);

        Session session = CassandraConnectionManager.getSession(ConfigPath);
        assert session != null;

        String cassandra_node = (String) ((JSONObject)jsonObject.get("cassandra_config")).get("cassandra.nodes");
        String percona_node = (String) ((JSONObject)jsonObject.get("percona_config")).get("mysql.server.connection.url");
        String table = (String) ((JSONObject)jsonObject.get("query")).get("table");
        String[] reqdColumns=((String) ((JSONObject)jsonObject.get("query")).get("reqdColumns")).split(",");
        String agg_value = (String) ((JSONObject)jsonObject.get("query")).get("aggregation_value");
        String startTime = (String) ((JSONObject)jsonObject.get("query_time")).get("start_time");
        String endTime = (String) ((JSONObject)jsonObject.get("query_time")).get("end_time");
        //String destFile = (String) ((JSONObject)jsonObject.get("query")).get("resultFileName");
        int kpi_id_location = Integer.parseInt((String) ((JSONObject)jsonObject.get("query")).get("kpi_id_location"));
        String cassandra_timeZone = (String) ((JSONObject)jsonObject.get("query_time")).get("cassandra_timeZone");
        String account_timeZone = (String) ((JSONObject)jsonObject.get("query_time")).get("account_timeZone");
        ArrayList<String[]> kpiDetails = new ArrayList<>();
        if(kpi_id_location > 0){
            String mst_id = (String) ((JSONObject)jsonObject.get("mst_head")).get("kpi_id_header");
            String mst_name = (String) ((JSONObject)jsonObject.get("mst_head")).get("kpi_name_header");
            kpiDetails.add(new String[]{mst_id,mst_name});
            List<KpiDetailsBean> a1 = null;
            try {
               a1 = getKpiList(ConfigPath);
            }catch(Exception e){
                System.out.println(e);
                System.out.println("Percona may not be running on "+percona_node.split("//?")[1]+" server or Password may be wrong. Please check.");
                System.exit(1);
            }
            for(int i =0; i< a1.size();i++){
                String[] a =  new String[]{String.valueOf(a1.get(i).getId()),a1.get(i).getName()};
               kpiDetails.add(a);
            }
        }

        List<String[]> rawRecords =  executeStatement(session,table,reqdColumns,agg_value,startTime,endTime,cassandra_node);
//        printData(rawRecords,1);
//        printData(kpiDetails,2);
        try {
            DataBreakage.mainDB(rawRecords,ConfigPath,kpiDetails,kpi_id_location,
                    resultPath, table, cassandra_timeZone,account_timeZone);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // System.exit(1);
        session.close();
    }
    
    private static JSONObject readConfig(String arg) {
        String ConfigFile = arg;
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static ArrayList<String[]> executeStatement(Session session, String table, String[] reqdColumns, String agg_value, String startTime, String endTime, String cassandra_node) throws ParseException {
        String s = "";
        for(int i =0;i< reqdColumns.length-1;i++){
            s+="\""+reqdColumns[i]+"\""+",";
        }
        s+="\""+reqdColumns[reqdColumns.length-1]+"\"";
        BuiltStatement builtStatement = null;
        if(agg_value.equals("")){
            System.out.println("Please provide a value of aggregation level in \"aggregation_value\" key in \"config.json\"");
            System.exit(1);
        } else if(startTime.equals("") || endTime.equals("")){
            System.out.println("Start time or/and end time for query is not found. Hence getting full table data");
            builtStatement = (QueryBuilder. select(s)
                    .from(table)
                    .where(QueryBuilder.eq("agg_level", Integer.parseInt(agg_value)))
                    .allowFiltering());
        } else {
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date D1 = f.parse(startTime);
            Date D2 = f.parse(endTime);
            builtStatement = (QueryBuilder. select(s)
                    .from(table)
                    .where(QueryBuilder.eq("agg_level", Integer.parseInt(agg_value)))
                    .and(QueryBuilder.gte("time", D1))
                    .and(QueryBuilder.lte("time",D2))
                    .allowFiltering());
        }
        System.out.println(builtStatement);
        //System.exit(1);
        ResultSet snapshotList = null;
        try{
            snapshotList = session.execute(builtStatement.toString());
        }
        catch(Exception e){
            System.out.println(e);
            System.out.println("Cassandra may not be running on "+cassandra_node+". Please check.");
            System.exit(1);
        }
        List<Row> rows = snapshotList.all();
        //System.out.println(rows);
        List<ArrayList<String>> tempData = new ArrayList();
        ArrayList<String[]> rec = new ArrayList<>();
        rec.add(reqdColumns);
        for(int i =0;i<rows.size();i++){
            String a = rows.get(i).toString();
            //Change code here
            String[] b = a.substring(4,a.length()-1).split(",");

            String dateStr = b[b.length-1].trim();
            DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
            Date date = formatter.parse(dateStr);
            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
            //Date result = new Date(date.getTime());
            String currDate = simple.format(date);
            b[b.length-1] = currDate;
            rec.add(b);
        }
        return rec;
        //System.out.println(Arrays.toString(rec.get(10)));
//        System.exit(1);
//        for (Row row : rows) {
//            ArrayList<String> a = new ArrayList<>();
//            a.add(row.getString("account_id"));
//            a.add(row.getString("instance_id"));
//            a.add(row.getString("kpi_id"));
//            a.add(String.valueOf(row.getTimestamp("time")));
//            tempData.add(a);
//        }
//        for(int i =0;i<tempData.size();i++){
//            System.out.println(i+","+tempData.get(i));
//        }
    }
    public static List<KpiDetailsBean> getKpiList(String configPath){
        InitializeConstants.initializeConstants(configPath);
        KPIDataDao kpiDataDao = MySQLConnectionManager.getInstance().open(KPIDataDao.class);
        try {
            return kpiDataDao.getKpiList();
        }catch (Exception e){
            return new ArrayList<>();
        }finally {
            MySQLConnectionManager.getInstance().close(kpiDataDao);
        }
    }
    private static void printData(List<String[]> records, int a) {
        try {
            FileWriter fr = new FileWriter("/home/swagatm/IdeaProjects/DataBreakage/a"+a+".txt");
            for(int i = 0;i<records.size();i++){
                fr.write(i+"  "+ Arrays.toString(records.get(i)) +"\n");
                //System.out.println(i+"  "+records.get(i)[3]);
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
//class setBeans{
//    void setKPIBeans(String mst_id, String mst_name) {
//        KpiDetailsBean kp = new KpiDetailsBean();
//        kp.setId(mst_id);
//        kp.setName(mst_name);
//    }
//}