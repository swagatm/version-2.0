package com.appnomic.appsone;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class allData {
    static int timeLocation = 0;
    static int account_idLocation = 0;
    static int locHeadReqForTime = 0;
    static int kpi_idLocation = 0;
    public static List<String[]> generateBreakageData(List<String[]> records, List<String[]> kpiDetails, int kpi_idlocation, String table) {
        System.out.println("Entering generateBreakageData()");
            if(records.size()<=1){
                System.out.println("No data present in \""+table+"\" table of cassandra for the given time range.");
                System.exit(1);
            }
        for (int i = 0; i < records.size(); i++) {
            for (int j = 0; j < records.get(i).length; j++) {
                records.get(i)[j] = records.get(i)[j].trim();
            }
        }
        String[] reqdColumns=records.get(0);
        kpi_idLocation = kpi_idlocation;
        timeLocation = reqdColumns.length - 1;
        account_idLocation = 0;
        records.remove(0);
        sortAccount_id(0, records.size() - 1, records);
        int o= 0;
        for(o=0;o<reqdColumns.length-2;o++){
            records = preparesort_Id(records,o,o+1);
        }
        if(o == reqdColumns.length-2){
            records = preparesortTimeStamp(records,o);
        }
        String kpi_idRegex = "";
        String kpi_nameRegex = "";
        if(kpi_idLocation>=0){
            kpi_idRegex = kpiDetails.get(0)[0];
            kpi_nameRegex = kpiDetails.get(0)[1];
            if(kpiDetails.size()<=1) {
                System.out.println("--------------------------------------");
                System.out.println("****Kpi details are not loaded properly****");
                System.out.println("--------------------------------------");
            }
            changekpiName(records, kpiDetails, kpi_idRegex, kpi_nameRegex);
        }
        System.out.println("Exiting generateBreakageData()");
        return records;
    }

    private static void sortAccount_id(int low, int high, List<String[]> records) {
        System.out.println("Entering sortAccount_id()");
        String pivot = records.get((low + high) / 2)[account_idLocation];
        int i = low, j = high;
        while (i <= j) {
            while (records.get(i)[account_idLocation].compareTo(pivot) < 0) i++;
            while (records.get(j)[account_idLocation].compareTo(pivot) > 0) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortAccount_id(low, j, records);
        if (i < high) sortAccount_id(i, high, records);
        System.out.println("Exiting sortAccount_id() ");
    }

    private static List<String[]> preparesort_Id(List<String[]> records, int headerPre, int headerCurr) {
        System.out.println("Entering preparesort_Id() ");
        List<String[]> entries = new ArrayList<>();
        List<String[]> final1 = new ArrayList<>();
        int mm = 0;
        //entries.add(items1);
        for (int i = 0; i < records.size(); i++) {
            entries.add(records.get(i));
            String tmp = "";
            if (i == records.size() - 1) {
                tmp = records.get(i)[headerPre];
            } else {
                tmp = records.get(i + 1)[headerPre];
            }
            if (!records.get(i)[headerPre].equals(tmp) || i == records.size() - 1) {

                List<String[]> entriesRet = sort_Id(0, entries.size() - 1, entries,headerCurr);
                for (int u = 0; u < entriesRet.size(); u++) {
                    final1.add(mm, entriesRet.get(u));
                    mm++;
                }
                entries = new ArrayList<>();
            }

        }
        System.out.println("Exiting preparesortApplication_Id() ");
        return final1;
    }

    private static List<String[]> sort_Id(int low, int high, List<String[]> records, int headerCurr) {
        System.out.println("Entering sort_Id() ");
        String pivot ="";
        try{
            pivot = records.get((low + high) / 2)[headerCurr];
        }catch (Exception e){
            System.out.println(e);
            System.out.println(Arrays.toString(records.get((low + high) / 2)));
        }

        int i = low, j = high;
        while (i <= j) {
            while (records.get(i)[headerCurr].compareTo(pivot) < 0) i++;
            while (records.get(j)[headerCurr].compareTo(pivot) > 0) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sort_Id(low, j, records,headerCurr);
        if (i < high) sort_Id(i, high, records,headerCurr);
        System.out.println("Exiting sort_Id() ");
        return records;
    }

    private static List<String[]> preparesortTimeStamp(List<String[]> records, int headerPre) {
        System.out.println("Entering praparesortTimeStamp() ");
        locHeadReqForTime = headerPre;
        List<String[]> entries = new ArrayList<>();
        List<String[]> final1 = new ArrayList<>();
        int mm = 0;
        //entries.add(items1);
        for (int i = 0; i < records.size(); i++) {
            entries.add(records.get(i));
            String tmp = "";
            if (i == records.size() - 1) {
                tmp = records.get(i)[headerPre];
            } else {
                tmp = records.get(i + 1)[headerPre];
            }
            if (!records.get(i)[headerPre].equals(tmp) || i == records.size() - 1) {

                List<String[]> entriesRet = srttimeStmp(0, entries.size() - 1, entries);
                for (int u = 0; u < entriesRet.size(); u++) {
                    final1.add(mm, entriesRet.get(u));
                    mm++;
                }
//                for(int u = 0;u<final1.size();u++){
//                    System.out.println(final1.get(u)[0]+","+final1.get(u)[1]+","+final1.get(u)[2]+","+final1.get(u)[3]);
//                }
//                System.out.println("----------------------------");
                entries = new ArrayList<>();
            }

        }
        //timeshort(final1);
//       for (int i =0;i<final1.size();i++) {
//          for(int j = 0;j<4;j++){
//              System.out.print(final1.get(i)[j]+",");
//           }
//           System.out.println();
//      }
//        System.out.println("=============++++++++++++++++++++++++++++++++===============================");
        System.out.println("Exiting praparesortTimeStamp() ");

        //printData(final1);
        return final1;
    }

    private static List<String[]> srttimeStmp(int low, int high, List<String[]> entries) {
        System.out.println("Entering srttimeStmp() ");
//        for (int i = 0; i < entries.size(); i++) {
//            for (int j = 0; j < entries.get(0).length; j++) {
//                System.out.println(entries.get(i)[j]+",");
//            }
//        }
        for (int i = 0; i < entries.size(); i++) {
            String a = "";
            try{
                a = entries.get(i)[timeLocation].substring(0, 19);
            }catch (Exception e){
                System.out.println(e);
                System.out.println( entries.get(i)[timeLocation]);
            }

            //2020-06-27 09:15:00.000000+0000
            //System.out.println(a);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(a);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            long millis = date.getTime();

            //System.out.println(millis);
            entries.get(i)[timeLocation] = String.valueOf(millis);
            entries.get(i)[timeLocation] = entries.get(i)[timeLocation].substring(0, entries.get(i)[timeLocation].length() - 3);
            //System.out.println(records.get(i)[0]);
//                for(int j = 0;j<4;j++){
//                    System.out.print(records.get(i)[j]+",");
//                }
//                System.out.println();
        }
        sortTimeStamp(0, entries.size() - 1, entries);
        System.out.println("Exiting srttimeStmp() ");
        return entries;
    }

    public static void sortTimeStamp(int low, int high, List<String[]> records) {
        System.out.println("Entering sortTimeStamp() ");
        int pivot = Integer.parseInt(records.get((low + high) / 2)[timeLocation]);
        int i = low, j = high;
        while (i <= j) {
            while (Integer.parseInt(records.get(i)[timeLocation]) < pivot) i++;
            while (Integer.parseInt(records.get(j)[timeLocation]) > pivot) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortTimeStamp(low, j, records);
        if (i < high) sortTimeStamp(i, high, records);
        System.out.println("Exiting sortTimeStamp() ");
    }

    private static void changekpiName(List<String[]> records, List<String[]> kpiDetails, String kpi_idRegex, String kpi_nameRegex) {
        System.out.println("Entering changekpiName()");

        int kpi_idLoc = 0;
        for (int i =0;i<kpiDetails.get(0).length;i++) {
            if(kpiDetails.get(0)[i].matches(kpi_idRegex)) {
                kpi_idLoc = i;
            }
        }
        int kpi_nameLoc = 0;
        for (int i =0;i<kpiDetails.get(0).length;i++) {
            if(kpiDetails.get(0)[i].matches(kpi_nameRegex)) {
                kpi_nameLoc = i;
            }
        }
        for( int i = 0; i < records.size();i++){
            for(int j = 0; j< kpiDetails.size();j++){
                if(records.get(i)[kpi_idLocation].equals(kpiDetails.get(j)[kpi_idLoc])){
                    records.get(i)[kpi_idLocation] = kpiDetails.get(j)[kpi_nameLoc];
                }
            }
        }
        System.out.println("Exiting changekpiName()");
    }

    private static void printData(List<String[]> records, int a) {
        try {
            FileWriter fr = new FileWriter("C:\\Users\\HP\\Desktop\\transactionSorted"+a+".txt");
            for(int i = 1;i<records.size();i++){
                fr.write(i+"  "+ Arrays.toString(records.get(i)) +"\n");
                //System.out.println(i+"  "+records.get(i)[3]);
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

