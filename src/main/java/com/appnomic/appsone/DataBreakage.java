package com.appnomic.appsone;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataBreakage {
    public static void mainDB(List<String[]> rawRecords, String ConfigFile, List<String[]> kpiDetails, int kpi_idLocation, String DestFile, String table, String cassandra_timeZone, String account_timeZone) throws IOException {
        System.out.println("Entering main() ");
        List<String[]> records = allData.generateBreakageData(rawRecords,kpiDetails,kpi_idLocation, table);
        prepareWriteDataFile(records,DestFile,ConfigFile,cassandra_timeZone,account_timeZone);
        System.out.println("Exiting main()");
    }

    private static void prepareWriteDataFile(List<String[]> transactionRecords, String DestFile, String ConfigFile, String cassandra_timeZone, String account_timeZone) {
        System.out.println("Entering prepareWriteDataFile()");
        Path path = Paths.get(DestFile);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        String[] reqdColumns=((String) ((JSONObject)jsonObject.get("query")).get("reqdColumns")).split(",");
        HashMap<Integer, String> hmap = new HashMap<Integer, String>();
        String a = "";
        for(int i =0;i<reqdColumns.length-1;i++){
            hmap.put(i,reqdColumns[i]);
        }
        Set set = hmap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            a+=mentry.getValue()+", ";
//            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
//            System.out.println(mentry.getValue());
        }

//        System.out.println(hmap.values().toString());
//        for (int i =0;i<hmap.size();i++){
//            System.out.println(hmap.keySet().toArray()[i]);
//        }
        appendUsingFileWriter(DestFile, "DataBreakage(Minutes), "+a+"StartTime, EndTime\n");
        //System.exit(1);
        List<String[]> entries = new ArrayList<>();
        List<String[]> timeDiffList = new ArrayList<>();
        for (int i = 0; i < transactionRecords.size(); i++) {
            entries.add(transactionRecords.get(i));
            String tmp = "";
            if (i == transactionRecords.size()-1) {
                tmp = transactionRecords.get(i)[allData.locHeadReqForTime];
            } else {
                tmp = transactionRecords.get(i + 1)[allData.locHeadReqForTime];
            }
            if (!transactionRecords.get(i)[allData.locHeadReqForTime].equals(tmp) || i==transactionRecords.size()-1 ) {
                writeData(entries, timeDiffList, hmap, cassandra_timeZone,account_timeZone);
                entries = new ArrayList<>();
            }
        }

        for (int i = 0; i < timeDiffList.size(); i++) {
            for (int j = 0; j < timeDiffList.get(i).length - 1; j++){
                appendUsingFileWriter(DestFile, timeDiffList.get(i)[j] + ", ");
            }
            appendUsingFileWriter(DestFile,timeDiffList.get(i)[timeDiffList.get(i).length - 1]+"\n");
        }


//        for (int i = 0; i < timeDiffList.size(); i++) {
//            for (int j = 0; j < timeDiffList.get(i).length; j++)
//            System.out.print(timeDiffList.get(i)[j]+", ");
//            System.out.println();
//        }
        System.out.println("Exiting prepareWriteDataFile()");
    }
    private static void writeData(List<String[]> records, List<String[]> timeDiffList, HashMap<Integer, String> hmap, String cassandra_timeZone, String account_timeZone) {
        System.out.println("Entering writeData()");
        //System.out.println(accRef+","+insRef+","+kpiRef);

        List<ArrayList<String>> withoutBreakagesorted = new ArrayList<>();
        for (int i = 0; i < records.size() - 1; i++) {
            int diff = Integer.parseInt(records.get(i + 1)[allData.timeLocation]) - Integer.parseInt(records.get(i)[allData.timeLocation]);
            int diffInMinutes = diff / 60;
            long miliSec1 = Long.parseLong(records.get(i)[allData.timeLocation] + "000");
            long miliSec2 = Long.parseLong(records.get(i + 1)[allData.timeLocation] + "000");
            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
            Date result1 = new Date(miliSec1);
            Date result2 = new Date(miliSec2);
            String currDate = simple.format(result1);
            String nextDate = simple.format(result2);
            currDate=changeTimeZone(currDate,cassandra_timeZone,account_timeZone);
            nextDate=changeTimeZone(nextDate,cassandra_timeZone,account_timeZone);
            if (diff > 60) {
                ArrayList<String> sample = new ArrayList<>();
                sample.add(String.valueOf(--diffInMinutes));
                Set set = hmap.entrySet();
                Iterator iterator = set.iterator();
                while(iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry)iterator.next();
//            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
//            System.out.println(mentry.getValue());
                    sample.add(records.get(i)[(int) mentry.getKey()]);
                }
                sample.add(currDate);
                sample.add(nextDate);
                withoutBreakagesorted.add(sample);
            }
        }
        if(withoutBreakagesorted.size()==0){
            return;
        }
//            for (int i =0;i<withoutBreakagesorted.size();i++){
//                System.out.println(Arrays.toString(withoutBreakagesorted.get(i)));
//            }
        //deep copy all record to nn arrayList
        ArrayList<String[]> nn = new ArrayList<>();
        for(int i =0;i<withoutBreakagesorted.size();i++){
            String[] arr = new String[withoutBreakagesorted.get(i).size()];
            for(int j =0;j<arr.length;j++){
                arr[j] = withoutBreakagesorted.get(i).get(j);
            }
            nn.add(arr);
        }
//        for(int i =0;i<nn.size();i++){
//            System.out.println(Arrays.toString(nn.get(i)));
//        }
        sortBreakageTimeStamp(0, withoutBreakagesorted.size()-1,nn);

        for (int i =  withoutBreakagesorted.size() -1; i >= 0; i--) {
            timeDiffList.add(nn.get(i));
        }
        System.out.println("Exiting writeData()");
    }
    private static void sortBreakageTimeStamp(int low, int high,ArrayList<String[]> withoutBreakagesorted) {
        System.out.println("Entering sortBreakageTimeStamp()");
        int pivot = Integer.parseInt(withoutBreakagesorted.get((low + high) / 2)[0]);
        int i = low, j = high;
        while (i <= j) {
            while (Integer.parseInt(withoutBreakagesorted.get(i)[0]) < pivot) i++;
            while (Integer.parseInt(withoutBreakagesorted.get(j)[0])> pivot)  j--;
            if (i <= j) {
                String[] temp = withoutBreakagesorted.get(i);
                withoutBreakagesorted.set(i, withoutBreakagesorted.get(j));
                withoutBreakagesorted.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortBreakageTimeStamp(low, j, withoutBreakagesorted);
        if (i < high) sortBreakageTimeStamp(i, high, withoutBreakagesorted);
        System.out.println("Exiting sortBreakageTimeStamp() ");
    }
    private static void appendUsingFileWriter(String filePath, String text) {
        System.out.println("Entering appendUsingFileWriter() ");
        File file = new File(filePath);
        FileWriter fr = null;
        try {
            // Below constructor argument decides whether to append or override
            fr = new FileWriter(file, true);
            fr.write(text);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Exiting appendUsingFileWriter() ");
    }
    private static String changeTimeZone(String time, String cassandra_timeZone, String account_timeZone){
        System.out.println("Entering changeTimeZone() ");
        String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        TimeZone tz = TimeZone.getTimeZone(cassandra_timeZone);
        formatter.setTimeZone(tz);
        String dateInString = time;
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatterFinal = new SimpleDateFormat(DATE_FORMAT);
        TimeZone tzFinal = TimeZone.getTimeZone(account_timeZone);
        formatterFinal.setTimeZone(tzFinal);
        System.out.println("Exiting changeTimeZone() ");
        return formatterFinal.format(date);
    }
}

